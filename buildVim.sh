git clone https://github.com/vim/vim.git ~/vimbuild
cd ~/vimbuild/src
./configure --enable-pythoninterp --enable-python3interp --enable-gui --enable-luainterp --with-features=huge --prefix=/home/mauricio/apps/vim
make
make install
make clean
cp -n  ~/.bash_aliases ~/.bash_aliases_vimbuildbkp
echo alias vim=~/apps/vim/bin/vim >> ~/.bash_aliases
. ~/.bashrc
